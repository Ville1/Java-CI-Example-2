import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author villemyl
 */
public class ExampleTest {
    
    public ExampleTest() {
        //Nothing!
    }

    /**
     * Test of Store_Number method, of class Example.
     */
    @Test
    public void Test_Store_Number() {
        Example example = new Example();
        
        assertFalse(example.Store_Number(10));
        assertTrue(example.Store_Number(10));
        assertFalse(example.Store_Number(-10));
    }

    /**
     * Test of Get_Number method, of class Example.
     */
    @Test
    public void Test_Get_Number() {
        Example example = new Example();
        
        assertEquals(example.Get_Number(), -1);
        
        example.Store_Number(7);
        assertEquals(example.Get_Number(), 7);
        
        example.Store_Number(-100);
        assertEquals(example.Get_Number(), 7);
    }
}
