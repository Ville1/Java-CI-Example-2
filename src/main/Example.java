/**
 *
 * @author Ville Myllykangas
 */
public class Example {
    protected int number;
    
    public Example() {
        number = -1;
    }
    
    /**
     * Stores positive number in object
     * @param new_number Number to be stored, if negative number is given it will not be stored.
     * @return Was last number replaced?
     */
    public boolean Store_Number(int new_number) {
        boolean number_was_replaced = false;
        
        if(new_number < 0)
            return false;
        
        if(number != -1)
            number_was_replaced = true;
        
        number = new_number;
        return number_was_replaced;
    }
    
    /**
     * Returns stored number
     * @return 
     */
    public int Get_Number() {
        return number;
    }
}